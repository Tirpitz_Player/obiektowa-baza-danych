#!/usr/bin/enc python3

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2021, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "The MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = "TBA"

import persistent


class Account(persistent.Persistent):
    """ Account class """
    def __init__(self, firstname: str, lastname: str) -> None:
        """
        Constructor
        :param firstname: Owner first name
        :param lastname: Owner last name
        """
        self.__balance = 0.0
        self.__owner_firstname = firstname
        self.__owner_lastname = lastname

    def __owner_full_name(self) -> str:
        """
        Return full owner name
        :return: Firstname with lastname of owner
        """
        return "{0} {1}".format(self.__owner_firstname, self.__owner_lastname)

    def withdraw(self, amount_of_money: float) -> None:
        """
        Withdraw money from user account
        :param amount_of_money: Amount of money to withdraw from account
        :return: None
        """
        try:
            if self.__balance - amount_of_money >= 0:
                self.__balance -= amount_of_money
            else:
                print("{0} doesn't have enough money to withdraw".format(self.__owner_full_name()))
        except Exception as err:
            print("Something goes wrong", err)

    def deposit(self, amount_of_money: float) -> None:
        """
        Deposit money to user account
        :param amount_of_money: Amount of money to deposit from account
        :return: None
        """
        try:
            self.__balance += amount_of_money
        except Exception as err:
            print("Something goes wrong", err)

    def get_account_info(self) -> str:
        """
        Return account information
        :return: Full name of owner and his balance
        """
        return "{0} has {1} PLN".format(self.__owner_full_name(), self.__balance)
