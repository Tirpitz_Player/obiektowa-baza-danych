#!/usr/bin/enc python3

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2021, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "The MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = "TBA"

import ZODB.FileStorage
import transaction
from account import Account


class SaveToDatabase:
    """ Save object to database """
    def __init__(self, root: ZODB.DB, oid: str, obj: Account) -> None:
        """
        Constructor
        :param root: database connection object
        :param oid: id of element in database
        :param obj: object to save
        """

        # Zapisywanie obiektu do bazy danych
        root[oid] = obj

        # Zapisywanie bazy danych do pliku
        transaction.commit()


class LoadFromDatabase:
    """ Load object from database by IOD """
    def __init__(self, root: ZODB.DB, oid: str):
        """
        Constructor
        :param root: database connection object
        :param oid: id of element in database
        """
        self.__object = root[oid]
        self.__root = root

    def get_object(self) -> Account:
        """
        Get object
        :return: object from db
        """
        return self.__object

    def print_all_data(self) -> None:
        """ Print all accounts from db """
        for account in self.__root.values():
            print(account)

