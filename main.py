#!/usr/bin/enc python3

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2021, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "The MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = "TBA"

import ZODB.FileStorage
from account import Account
from db import *
import os

# Zmienne
db_file_name = 'db.fs'

# Usuwanie bazy danych
try:
    os.remove(db_file_name)
except Exception as err:
    print(err)


# Tworzenie bazy danych
storage = ZODB.FileStorage.FileStorage(db_file_name)
db = ZODB.DB(storage)
connection = db.open()
root = connection.root()

# Tworzenie obiektu
mikolaj = Account("Mikołaj", "Błażejewski")
mikolaj.deposit(100)
blazej = Account("Błażej", "Chojaczyk")
blazej.deposit(200)
print("Przed zapisaniem:", mikolaj.get_account_info())

# Zapisywanie obiektu do bazy danych
SaveToDatabase(root, 'account-1', mikolaj)
SaveToDatabase(root, 'account-2', blazej)

# Wczytywanie danych z bazy danych
load_data = LoadFromDatabase(root, 'account-1')
mikolaj_db = load_data.get_object()

# Wyswietlanie objektu z bazy danych
print("Po wczytaniu:", mikolaj_db.get_account_info())

# Wyswietlanie wszystkich obiektów z bazy danych
print('All elements in db:')
load_data.print_all_data()

# Zamykanie bazy danych
db.close()
